import os

import yaml


def get_value(key):
    current_path = os.path.dirname(os.path.realpath(__file__))
    path = os.path.join(current_path, "data.yaml")
    f = open(path, 'r', encoding='utf-8')
    config = yaml.load(f.read(), Loader=yaml.CLoader)
    return config[key]


# if __name__ == '__main__':
#     current_path = os.path.dirname(os.path.realpath(__file__))
#     path = os.path.join(current_path, "data.yaml")
#     # print(path)
#     f = open(path, 'r', encoding='utf-8')
#     x = yaml.load(f.read(), Loader=yaml.CLoader)
#     print(x['url'])
#     print(x.get('url'))