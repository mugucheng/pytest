import time

from selenium.webdriver.common.by import By

from base.base_page import BasePage


class LoginPage(BasePage):
    # 页面元素定位
    username_loc = (By.XPATH, '//*[@id="user_login"]')
    password_loc = (By.XPATH, '//*[@id="user_password"]')
    submit_loc = (By.XPATH, '//*[@id="new_user"]/div/div/div/div[4]/input')

    def login(self, url, username, password):
        self.get(url)
        self.send_keys(self.username_loc, username)
        self.send_keys(self.password_loc, password)
        self.click(self.submit_loc)
        time.sleep(5)
