import time

from selenium.webdriver.common.by import By

from base.base_page import BasePage


class InstitutionPage(BasePage):
    # 页面元素定位
    jigou = (By.XPATH, '//*[@id="rc-users__container"]/div[1]/div[2]/div/div[1]/div[1]/div[1]/div[2]/a[1]')
    jigou2 = (By.XPATH, '//*[@id="rc-users__container"]/div[2]/div[1]/div/a[1]')
    sousuo = (By.XPATH, '//*[@id="rc-users__container"]/div[2]/div[1]/div/a[2]')

    def cx(self):
        self.click(self.jigou)
        self.click(self.jigou2)
        self.click(self.sousuo)
        time.sleep(3)
