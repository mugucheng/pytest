from selenium.webdriver.support.select import Select


class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()

    # 打开网页
    def get(self, url):
        # self.driver.maximize_window()
        self.driver.get(url)

    # 定位元素
    def locate_element(self, args):
        return self.driver.find_element(*args)

    # 设置值
    def send_keys(self, args, value):
        self.locate_element(args).send_keys(value)

    # 单击
    def click(self, args):
        self.locate_element(args).click()

    # 进入框架
    def goto_frame(self, frame_name):
        self.driver.switch_to.frame(frame_name)

    # 出框架
    def out_frame(self, frame_name):
        self.driver.switch_to.default_content(frame_name)

    # 选择下拉框
    def choice_select_by_value(self, args, value):
        sel = Select(self.locate_element(args))
        sel.select_by_index(value)

    # 关闭
    def close(self):
        self.driver.close()
