import pytest

from pageobject.institution_page import InstitutionPage
from pageobject.login_page import LoginPage
from config.read_config import get_value


@pytest.fixture(scope='class')
def jg(driver1):
    jg = InstitutionPage(driver1)
    return jg


class TestInstitution:

    def test_cx(self, jg):
        jg.cx()
