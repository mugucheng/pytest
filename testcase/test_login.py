import pytest

from pageobject.login_page import LoginPage
from config.read_config import get_value


@pytest.fixture(scope='class')
def lg(driver1):
    lg = LoginPage(driver1)
    return lg


class TestLogin:

    @pytest.mark.run(order=0)
    def test_login(self, lg):
        lg.login(get_value('url'), get_value('username'), get_value('password'))
