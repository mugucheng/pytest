import pytest
from selenium import webdriver


@pytest.fixture(scope="session")
def driver1(request):  # request是Pytest中的一个关键字，固定写法。
    # 步骤1：创建浏览器驱动对象
    driver = webdriver.Chrome()

    # 步骤3：定义用例执行后要执行的代码，封装到一个函数中
    def end():  # 这个end函数命是自定义的
        driver.quit()

    # 步骤4：执行上面封装的代码。
    # 通过request关键字，结束上面的函数。
    request.addfinalizer(end)  # 终结函数

    # 步骤2：返回浏览器驱动对象，给测试用例
    # 返回参数可以是变量，对象，表达式，常量值。
    # 返回参数的名称和可以和fixture方法的名称相同。
    return driver
